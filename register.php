<?php include 'connection.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>

<body>
    <center>
        <h1>Storing Students data in Database</h1>
        <form action="insert.php" method="post">

            <p>
                <label for="firstName">First Name:</label>
                <input type="text" name="first_name" id="firstName">
            </p>

            <p>
                <label for="lastName">Last Name:</label>
                <input type="text" name="last_name" id="lastName">
            </p>

            <p>
                <label for="standard">Standard:</label>
                <input type="number" min="1" name="standard" id="standard">
            </p>

            <p>
                <label for="percentage">Percentage:</label>
                <input type="text" name="percentage" id="percentage">
            </p>

            <p>
                <label for="interset">Interest:</label>
                <input type="text" name="interest" id="interest">
            </p>

            <p>
                <label for="presentAbsent">Present Absent:</label>
                <input type="text" name="presence_absence" id="presentAbsent">
            </p>

            <button type="submit">Submit</button>
        </form>
    </center>
</body>
<?php
$conn->close();
?>
</html>