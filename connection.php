<?php
$servername = "127.0.0.1";
$username = "root";
$password = "root";
$dbname = "practical22";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// sql to create table
// $sql = "CREATE TABLE students (
//   id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//   first_name VARCHAR(50) NOT NULL,
//   last_name VARCHAR(50) NOT NULL,
//   standard Integer(50),
//   percentage Float NOT NULL,
//   interest VARCHAR(50) NOT NULL
//   )";

// $sql = "CREATE TABLE student_attendances (
//   id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//   student_id INT UNSIGNED NOT NULL,
//   created_at DATE NOT NULL,
//   presence_absence VARCHAR(50) NOT NULL,
//   FOREIGN KEY (student_id)
//       REFERENCES students (id)
//       ON DELETE CASCADE
//   )";

// if ($conn->query($sql) === TRUE) {
//   echo "Table students created successfully";
// } else {
//   echo "Error creating table: " . $conn->error;
// }

?>