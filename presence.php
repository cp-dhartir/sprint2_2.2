<?php include 'connection.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Presence</title>
</head>

<body>
    <center>
        <h1>Storing Attendance data in Database</h1>
        <form action="attendance.php" method="post">

            <p>
                <label for="student">Student:</label>
                <input type="number" min="1" name="student_id" id="student">
            </p>

            <p>
                <label for="date">Date:</label>
                <input type="date" name="created_at" id="created_at">
            </p>

            <p>
                <label for="presentAbsent">Present Absent:</label>
                <input type="text" name="presence_absence" id="presentAbsent">
            </p>

            <button type="submit">Submit</button>
        </form>
    </center>
</body>
<?php
$conn->close();
?>
</html>