<?php

include('connection.php');

if (isset($_POST['date']) && $_POST['date'] != null) {

    $id = $_POST['id'];
    $date = $_POST['date'];
    $sql = "SELECT * FROM students JOIN student_attendances ON students.id = student_attendances.student_id WHERE student_attendances.created_at = '$date' AND student_attendances.student_id = '$id'";
} else if (isset($_POST['total']) && $_POST['total'] != null) {

    $sql = "SELECT s.*, sa.student_id , count(case when sa.presence_absence ='a' then 1 end) as absent_count
        , count(case when sa.presence_absence ='p' then 1 end) as present_count FROM student_attendances as sa , students as s where s.id = sa.student_id group by sa.student_id";
} else if (isset($_POST['absent']) && $_POST['absent'] != null) {

    $sql = "SELECT * FROM students JOIN student_attendances ON students.id = student_attendances.student_id WHERE students.percentage < 70 AND student_attendances.presence_absence = 'a'";
} else {

    $sql = "SELECT * FROM students JOIN student_attendances ON students.id = student_attendances.student_id";
}

$row = mysqli_query($conn, $sql);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>

<body>
    <div style="display: flex;">

        <div style="margin-right: 30px;">

            <h1>Search Form</h1>
            <form action="index.php" method="post">
                <label>Find student's presence/absence on a particular day</label> <br>
                id : <input type="number" name="id" id="id"> <br> <br>
                <input type="date" name="date" id="date"> <br> <br>

                <input type="checkbox" name="total" id="total">
                <label>Find total absence/presence of every student</label> <br> <br>

                <input type="checkbox" name="absent" id="absent">
                <label>Find absent students with a percentage lower than 70</label> <br> <br>

                <button type="submit">Submit</button>
            </form>
        </div>
        <center>
            <h1>Students data</h1>
            <h3><?php echo "$row->num_rows found <br>";
                if (!empty($ans)) {
                    echo "Absent : " . $ans['absent_count'] . " Present : " . $ans['present_count'];
                }
                ?></h3>
            <table border="2">
                <thead>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Standard</th>
                    <th>Percentage</th>
                    <th>Interest</th>
                    <?php if (isset($_POST['total']) && $_POST['total'] != null) { ?>
                        <th>Absence</th>
                        <th>Presence</th>
                    <?php } else { ?>
                        <th>Date</th>
                        <th>Attendance</th>
                    <?php } ?>
                </thead>
                <tbody>
                    <?php foreach ($row as $value) {
                    ?>
                        <tr>
                            <td><?php echo $value['student_id']; ?></td>
                            <td><?php echo $value['first_name']; ?></td>
                            <td><?php echo $value['last_name']; ?></td>
                            <td><?php echo $value['standard']; ?></td>
                            <td><?php echo $value['percentage']; ?></td>
                            <td><?php echo $value['interest']; ?></td>
                            <?php if (isset($_POST['total']) && $_POST['total'] != null) { ?>
                                <td><?php echo $value['absent_count']; ?></td>
                                <td><?php echo $value['present_count']; ?></td>
                            <?php } else { ?>
                                <td><?php echo date("Y-m-d", strtotime($value['created_at'])); ?></td>
                                <td><?php echo $value['presence_absence']; ?></td>
                            <?php } ?>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </center>
    </div>
</body>
<?php
$conn->close();
?>
</html>